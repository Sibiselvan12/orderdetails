
import React from 'react';
import { Link } from 'react-router-dom';

export default class Edit extends React.Component {
    constructor(props) {
      super(props)
      this.state = {
       studentDetails: []
        }
    }

handleChange = (event) => {
    let studentDetails = this.state.studentDetails;
    const key = event.target.name;
    studentDetails[key] = event.target.value;
    console.log( studentDetails[key])
    this.setState({ studentDetails });

}
getstudentDetails = (id) => {
    console.log("++++++++++++++++++++"+ id );
    fetch(`http://localhost:8080/order/get/${id}`)
        .then((response) => response.json())
        .then((postResponse) => {
            console.log("++++++++++++++++++++"+ JSON.stringify(postResponse));
        this.setState({ studentDetails: postResponse })
        }).catch((error) => {
            console.log(error);
        });
}
SubmitEditedForm = (event) => {       
    fetch(`http://localhost:8080/students/updateStudent/${this.props.match.params.id}`, {
        method: 'PUT',
        body: JSON.stringify(this.state.studentDetails),
        headers: {
            'Content-type': 'application/json; charset=UTF-8',
        },
    }).then((response) => response.json())
        .then((json) => {
            console.log(json);
            alert("Content Submitted Succesfully");
            
            this.props.history.push("/");
        }).catch((error) => {
            console.log(error);
        });
}
componentDidMount() {
    console.log(this.props.match.params.id);
    this.getstudentDetails(this.props.match.params.id);


}
// render() {
//     const { studentDetails } = this.state;
//     return (
//         <div>
//         <div id="mySidenav" class="sidenav">
//     <div1>
//     <Link to="/"> <h1>CREATIVE TIM </h1></Link><br></br>
//    <i class="fa fa-home" aria-hidden="true">&nbsp; DASHBOARD</i><br></br>
//   {/* <a href="abc">Dashboard</a> */}
  
//   <i class="fa fa-bell" aria-hidden="true">&nbsp; NOTIFICATIOS</i><br></br>
//   <Link to="/user"> <i class="fa fa-user" aria-hidden="true">&nbsp; USER PROFILE</i></Link>
//   <i class="fa fa-table" aria-hidden="true">&nbsp; TABLE LIST</i>
// </div1>
// </div>

//       <form  onSubmit={(event) => this.SubmitEditedForm(event)}>

//         <h1 className="heading">Student Details</h1>
//         <label className="name2">Name <span class="required-field"></span> </label> <input type="text" id="p1" value={studentDetails.studentName} className="input2" name="studentName" placeholder="Enter your Name" name="studentName" onChange={this.handleChange} />
//         <label className="name2">Age <span class="required-field"></span></label>   <input type="text" id="p2" value={studentDetails.studentAge} className="input2"name="studentAge" placeholder="Enter your Age" name="studentAge" onChange={this.handleChange} />
//         <label className="name2">Date of Birth <span class="required-field"></span></label> <input type="date" id="p3" value={studentDetails.studentAttendance} className="input2" name="studentAttendance" name="studentDateofBirth" onChange={this.handleChange} />
//         <label className="name2">Phone Number <span class="required-field"></span></label><input type="number" id="p4" value={studentDetails.studentPhoneNumber} className="input2" name="studentPhoneNumber" placeholder="Enter your Phone Number" name="studentphoneNumber" onChange={this.handleChange} />
//         <label className="name2">Year of Passing <span class="required-field"></span></label> <input type="date" id="p5" value={studentDetails.studentDateofjoin} className="input2" name="studentDateofjoin" onChange={this.handleChange} />
//         <label className="name2">Student Degeree <span class="required-field"></span></label>
//         <select id="p6" name="studentDegeree" className="input2" value={studentDetails.studentDegeree} name ="studentDegeree" onChange={this.handleChange}>
//           <option value="UG"> UG</option>
//           <option value="PG"> PG</option>
//           <option value="Diplomo" >Diplomo</option>
//           {/* value={this.state.studentDegeree} onChange={this.handleDegreechange} */}
//         </select>
//         <label className="name2">Student Departement <span class="required-field"></span></label>
//         <select id="p7" className="input2" value={studentDetails.studentDepartement} name = "studentDepartement"onChange={this.handleChange}>
//           <option value="Bsc"> Bsc</option>
//           <option value="Eng"> Eng</option>
//           <option value="Archi">Archi</option>
//         </select><br></br>
//        <button id="b1" class="btn btn-primary"type='submit' >Submit</button>&nbsp;&nbsp;&nbsp;
//         <button className="button1"class="btn btn-secondary" onClick={() => this.reset()}>Reset</button>

//       </form>
//       </div>


//     );
//   }
}