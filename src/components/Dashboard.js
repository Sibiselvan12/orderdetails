import { Collapse, Dropdown } from "bootstrap";
import React from "react";
// react plugin used to create charts
import { Line, Pie } from "react-chartjs-2";
import { Link } from 'react-router-dom';
import logo from "../assets/logo192.png";
// reactstrap components
import {
  Card,
  CardHeader,
  CardBody,
  CardFooter,
  CardTitle,
  Row,
  Col,
  DropdownItem,
  NavItem,
  DropdownMenu,
  Nav,
  DropdownToggle,
  Input,
  InputGroupAddon,
  InputGroupText,
  NavbarToggler,
  InputGroup,
  NavbarBrand,
  Container,
} from "reactstrap";
// core components
import {
  dashboard24HoursPerformanceChart,
  dashboardEmailStatisticsChart,
  dashboardNASDAQChart,
} from "./variables/charts";


class Dashboardmain extends React.Component {
  render() {
    return (


      <div className="row">
        
        <p1>Dashboard</p1>


        {/* <Container fluid>
          <div className="navbar-wrapper">
            <div className="navbar-toggle">
              <button
                type="button"
                ref={this.sidebarToggle}
                className="navbar-toggler"
                onClick={() => this.openSidebar()}
              >
                <span className="navbar-toggler-bar bar1" />
                <span className="navbar-toggler-bar bar2" />
                <span className="navbar-toggler-bar bar3" />
              </button>
            </div>
            <NavbarBrand href="/">{this.getBrand()}</NavbarBrand>
          </div>
          <NavbarToggler onClick={this.toggle}>
            <span className="navbar-toggler-bar navbar-kebab" />
            <span className="navbar-toggler-bar navbar-kebab" />
            <span className="navbar-toggler-bar navbar-kebab" />
          </NavbarToggler>
          <Collapse
            isOpen={this.state.isOpen}
            navbar
            className="justify-content-end"
          >
            <form>
              <InputGroup className="no-border">
                <Input placeholder="Search..." />
                <InputGroupAddon addonType="append">
                  <InputGroupText>
                    <i className="nc-icon nc-zoom-split" />
                  </InputGroupText>
                </InputGroupAddon>
              </InputGroup>
            </form>
            <Nav navbar>
              <NavItem>
                <Link to="#pablo" className="nav-link btn-magnify">
                  <i className="nc-icon nc-layout-11" />
                  <p>
                    <span className="d-lg-none d-md-block">Stats</span>
                  </p>
                </Link>
              </NavItem>
              <Dropdown
                nav
                isOpen={this.state.dropdownOpen}
                toggle={(e) => this.dropdownToggle(e)}
              >
                <DropdownToggle caret nav>
                  <i className="nc-icon nc-bell-55" />
                  <p>
                    <span className="d-lg-none d-md-block">Some Actions</span>
                  </p>
                </DropdownToggle>
                <DropdownMenu right>
                  <DropdownItem tag="a">Action</DropdownItem>
                  <DropdownItem tag="a">Another Action</DropdownItem>
                  <DropdownItem tag="a">Something else here</DropdownItem>
                </DropdownMenu>
              </Dropdown>
              <NavItem>
                <Link to="#pablo" className="nav-link btn-rotate">
                  <i className="nc-icon nc-settings-gear-65" />
                  <p>
                    <span className="d-lg-none d-md-block">Account</span>
                  </p>
                </Link>
              </NavItem>
            </Nav>
          </Collapse>
        </Container>
         */}

       
        <div class="navbar">
          {/* <a class="active" href="#"><i class="fa fa-fw fa-home"></i> Home</a> */}

          {/* <a href="#"><i class="fa fa-fw fa-envelope"></i> Contact</a>
  <a href="#"><i class="fa fa-fw fa-user"></i> Login</a> */}

          {/* <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-gavel"></i>
                            <p class="hidden-md hidden-lg">Actions</p>
                        </a>
                        <ul class="dropdown-menu">
                            <li><a href="#">Create New Post</a></li>
                            <li><a href="#">Manage Something</a></li>
                            <li><a href="#">Do Nothing</a></li>
                            <li><a href="#">Submit to live</a></li>
                            <li class="divider"></li>
                            <li><a href="#">Another Action</a></li>
                        </ul>
                    </li> */}
                      {/* <h>Dashboard</h> */}
          <form class="navbar-form navbar-left navbar-search-form" role="search">
           
                    <div class="input-group">
                    

                        <span class="input-group-addon"><i class="fa fa-search fa-2x"></i></span>
                        <input type="text" value="" class="form-control" placeholder="Search..."></input>
                    </div>
                </form>
          <i class="fa fa-list-ul fa-2x" aria-hidden="true"></i>
          <li class="dropdown">
            <a href="#"  data-toggle="dropdown">

              {/* <span class="notification">5</span> */}
              <i class="fa fa-bell fa-2x" aria-hidden="true"></i>
              {/* <p class="hidden-md hidden-lg">Notifications</p> */}
            </a>
            <ul class="dropdown-menu">
              {/* <li><a href="#">Notification 1</a></li>
              <li><a href="#">Notification 2</a></li>
              <li><a href="#">Notification 3</a></li>
              <li><a href="#">Notification 4</a></li> */}
              {/* <li><a href="#">Another notification</a></li> */}
            </ul>
          </li>
          <i class="fa fa-cog fa-2x" aria-hidden="true"></i>
          
          
          
          
          
          

    </div>
    
        

        <nav class="navbar navbar-default">
          <div class="container-fluid">
            <div class="navbar-header">
              {/* <button type="button" class="navbar-toggle" data-toggle="collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button> */}
              
            </div>
            <div class="collapse navbar-collapse">

              {/* <header class="navbar-form navbar-left navbar-search-form" role="search">
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-search"></i></span>
                  <input type="text" value="" class="form-control" placeholder="Search..."></input>
                </div>
              </header> */}

              <ul class="nav navbar-nav navbar-right">
                <li>
                  <a href="charts.html">
                    <i class="fa fa-line-chart"></i>
                    <p>Stats</p>
                  </a>
                </li>

                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <i class="fa fa-gavel"></i>
                    <p class="hidden-md hidden-lg">Actions</p>
                  </a>
                  <ul class="dropdown-menu">
                    <li><a href="#">Create New Post</a></li>
                    <li><a href="#">Manage Something</a></li>
                    <li><a href="#">Do Nothing</a></li>
                    <li><a href="#">Submit to live</a></li>
                    <li class="divider"></li>
                    <li><a href="#">Another Action</a></li>
                  </ul>
                </li>

                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <i class="fa fa-bell-o"></i>
                    <span class="notification">5</span>
                    <p class="hidden-md hidden-lg">Notifications</p>
                  </a>
                  <ul class="dropdown-menu">
                    <li><a href="#">Notification 1</a></li>
                    <li><a href="#">Notification 2</a></li>
                    <li><a href="#">Notification 3</a></li>
                    <li><a href="#">Notification 4</a></li>
                    <li><a href="#">Another notification</a></li>
                  </ul>
                </li>

                <li class="dropdown dropdown-with-icons">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <i class="fa fa-list"></i>
                    <p class="hidden-md hidden-lg">More</p>
                  </a>
                  <ul class="dropdown-menu dropdown-with-icons">
                    <li>
                      <a href="#">
                        <i class="pe-7s-mail"></i> Messages
                              </a>
                    </li>
                    <li>
                      <a href="#">
                        <i class="pe-7s-help1"></i> Help Center
                              </a>
                    </li>
                    <li>
                      <a href="#">
                        <i class="pe-7s-tools"></i> Settings
                              </a>
                    </li>
                    <li class="divider"></li>
                    <li>
                      <a href="#">
                        <i class="pe-7s-lock"></i> Lock Screen
                              </a>
                    </li>
                    <li>
                      <a href="#" class="text-danger">
                        <i class="pe-7s-close-circle"></i>
                                  Log out
                              </a>
                    </li>
                  </ul>
                </li>

              </ul>

            </div>

          </div>

        </nav>
        
        {/* <div className="sidebar-wrapper ps">
          <ul class="nav">
            <li class="active">
              <a class="nav-link active" href="/dasdoard"aria-aria-current="page">
                ::before
                <i class="nc-icon nc-bank">
                  ::before
                </i>
              </a>
            </li>
          </ul>
        </div> */}


        <div id="mySidenav" class="sidenav">
          <div>
            <Link to="/"> <img src={logo}></img><h1>CREATIVE TIM </h1></Link><br></br>
            
            <i class="fa fa-home" aria-hidden="true">&nbsp; DASHBOARD</i><br></br>
            {/* <a href="abc">Dashboard</a> */}
            {/* <i class="fa fa-diamond" aria-hidden="true">&nbsp; ICONS</i><br></br>
  <i class="fa fa-map-marker" aria-hidden="true">&nbsp; MAPS</i><br></br> */}
            <Link to="/notification"> <i class="fa fa-bell" aria-hidden="true">&nbsp;   NOTIFICATIOS</i><br></br></Link>
            <Link to="/user"> <i class="fa fa-user " aria-hidden="true">&nbsp; USER PROFILE</i></Link>
            <Link to="/table"> <i class="fa fa-table" aria-hidden="true">&nbsp; TABLE LIST</i></Link>
          </div>


          <nav class="navbar navbar-expand-lg navbar-light bg-dark"></nav>
        </div>


          
        <div className="content col-sm-12">
          <hr/>

          <Row>
            <Col lg="3" md="6" sm="6">
              <Card className="card-stats">
                <CardBody className="Cardbody">
                  <Row>
                    <Col md="4" xs="5">
                      <div className="icon-big text-center icon-warning">
                        <i class="fa fa-globe fa-5x" aria-hidden="true"></i>
                        <i className="nc-icon nc-globe text-warning" />
                      </div>
                    </Col>
                    <Col md="8" xs="7">
                      <div className="numbers">

                        <p className="card-category">Capacity</p>
                        <CardTitle tag="p">150GB</CardTitle>
                        <p />
                      </div>
                    </Col>
                  </Row>
                </CardBody>
                <CardFooter>
                  <hr />
                  <div className="stats">
                    <i className="fas fa-sync-alt" /> Update Now
                  </div>
                </CardFooter>
              </Card>
            </Col>
            <Col lg="3" md="6" sm="6">
              <Card className="card-stats">
                <CardBody>
                  <Row>
                    <Col md="4" xs="5">
                      <div className="icon-big text-center icon-warning">
                        <i className="nc-icon nc-money-coins text-success" />
                        <i class="fa fa-money fa-5x" id="d1" aria-hidden="true"></i>
                      </div>
                    </Col>
                    <Col md="8" xs="7">
                      <div className="numbers">
                        <p className="card-category">Revenue</p>
                        <CardTitle tag="p">$ 1,345</CardTitle>
                        <p />
                      </div>
                    </Col>
                  </Row>
                </CardBody>
                <CardFooter>
                  <hr />
                  <div className="stats">
                    <i className="far fa-calendar" /> Last day
                  </div>
                </CardFooter>
              </Card>
            </Col>
            <Col lg="3" md="6" sm="6">
              <Card className="card-stats">
                <CardBody>
                  <Row>
                    <Col md="4" xs="5">
                      <div className="icon-big text-center icon-warning">
                        <i className="nc-icon nc-vector text-danger" />
                        <i class="fa fa-empire fa-5x" id="d1" aria-hidden="true"></i>
                      </div>
                    </Col>
                    <Col md="8" xs="7">
                      <div className="numbers">
                        <p className="card-category">Errors</p>
                        <CardTitle tag="p">23</CardTitle>
                        <p />
                      </div>
                    </Col>
                  </Row>
                </CardBody>
                <CardFooter>
                  <hr />
                  <div className="stats">
                    <i className="far fa-clock" /> In the last hour
                  </div>
                </CardFooter>
              </Card>
            </Col>
            <Col lg="3" md="1" sm="1">
              <Card className="card-stats">
                <CardBody>
                  <Row>
                    <Col md="4" xs="5">
                      <div className="icon-big text-center icon-warning">
                        <i className="nc-icon nc-favourite-28 text-primary" />
                        <i class="fa fa-heart fa-5x" id="d1" aria-hidden="true"></i>
                      </div>
                    </Col>
                    <Col md="8" xs="7">
                      <div className="numbers">


                        <p className="card-category">Followers</p>
                        <CardTitle tag="p">+45K</CardTitle>

                        <p />
                      </div>
                    </Col>
                  </Row>
                </CardBody>
                <CardFooter>
                  <hr />
                  <div className="stats">

                    <i className="fas fa-sync-alt" /> Update now
                  </div>
                </CardFooter>
              </Card>
            </Col>
          </Row>
          <div1>
            <Row>
              <Col md="12">
                <Card>
                  <CardHeader>
                    <CardTitle tag="h5">Users Behavior</CardTitle>
                    <p className="card-category">24 Hours performance</p>
                  </CardHeader>
                  <CardBody>
                    <Line
                      data={dashboard24HoursPerformanceChart.data}
                      options={dashboard24HoursPerformanceChart.options}
                      width={400}
                      height={100}
                    />
                  </CardBody>
                  <CardFooter>
                    <hr />
                    <div className="stats">
                      <i className="fa fa-history" /> Updated 3 minutes ago
                  </div>
                  </CardFooter>
                </Card>
              </Col>
            </Row>
            <Row>
              <Col md="4">
                <Card>
                  <CardHeader>
                    <CardTitle tag="h5">Email Statistics</CardTitle>
                    <p className="card-category">Last Campaign Performance</p>
                  </CardHeader>
                  <CardBody>
                    <Pie
                      data={dashboardEmailStatisticsChart.data}
                      options={dashboardEmailStatisticsChart.options}
                    />
                  </CardBody>
                  <CardFooter>
                    <div className="legend">
                      <i className="fa fa-circle text-primary" /> Opened{" "}
                      <i className="fa fa-circle text-warning" /> Read{" "}
                      <i className="fa fa-circle text-danger" /> Deleted{" "}
                      <i className="fa fa-circle text-gray" /> Unopened
                  </div>
                    <hr />
                    <div className="stats">
                      <i className="fa fa-calendar" /> Number of emails sent
                  </div>
                  </CardFooter>
                </Card>
              </Col>
              <Col md="8">
                <Card className="card-chart">
                  <CardHeader>
                    <CardTitle tag="h5">NASDAQ: AAPL</CardTitle>
                    <p className="card-category">Line Chart with Points</p>
                  </CardHeader>
                  <CardBody>
                    <Line
                      data={dashboardNASDAQChart.data}
                      options={dashboardNASDAQChart.options}
                      width={400}
                      height={100}
                    />
                  </CardBody>
                  <CardFooter>
                    <div className="chart-legend">
                      <i className="fa fa-circle text-info" /> Tesla Model S{" "}
                      <i className="fa fa-circle text-warning" /> BMW 5 Series
                  </div>
                    <hr />
                    <div className="card-stats">
                      <i className="fa fa-check" /> Data information certified
                  </div>
                  </CardFooter>
                </Card>
              </Col>
            </Row>
          </div1>


        </div>

       </div>
       

    );
  }
}

export default Dashboardmain;
