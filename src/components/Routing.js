

import { BrowserRouter, Route, Switch } from "react-router-dom"
import App from "../App";
import Bootstraptab from "./Bootstrap";
import Dashboardmain from "./Dashboard";
import Nav from "./Nav";
import Notification from "./Notification";

import Tables from "./Tables";
import User from "./User";

function Routing() {
    return (
      
      
      <BrowserRouter>
      <Switch>
          <Route  exact path="/" component={App}/>
          <Route  path="/table" component={Bootstraptab}/>
          <Route path="/user" component={User}/>
          <Route path="/dashboard" component={Dashboardmain}/>
          <Route path="/notifcation" component={Notification}/>
          

          
      </Switch>
      </BrowserRouter>
  
    );
  }
  
  export default Routing;